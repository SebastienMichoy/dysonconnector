# Dyson Fan

> Dyson Fan Name: `PT4-EU-HKA3335A`  
> Password: `5+AVgxQI2zYN/lOWLw3hRSHtMXmPk3ugKDYvupwkD0SFt+X+OmRVmkVQsBgpHlsjZiWjp4pRQ9N5s4lQJrIxMA==`

The communication on the same local network is done via *MQTT*.  
Wireshark filter: `ip.addr==192.168.2.2 and ip.addr==192.168.2.3 and mqtt`

## Steps
1. *Software* connects to *Dyson*
1. *Software* subscribe to requests of *Dyson*
    - `455/PT4-EU-HKA3335A/status/current`
    - `455/PT4-EU-HKA3335A/status/faults`
    - `455/PT4-EU-HKA3335A/status/connection`
    - `455/PT4-EU-HKA3335A/status/software`
    - `455/PT4-EU-HKA3335A/status/summary`
1. *Software* asks *Dyson*'s current status
1. *Dyson* gave its status at connection
1. *Dyson* gave its status
1. *Dyson* gave its environment status
1. ...

## Calls

### Dyson → phone

#### Information about the Dyson

**Topic:** 455/PT4-EU-HKA3335A/status/connection

**Message:**
```JSON
{
    "msg": "HELLO",
    "time": "2017-11-25T14:26:41.003Z",
    "model": "X455",
    "version": "21.03.08",
    "protocol": "1.0.0",
    "serialNumber": "PT4-EU-HKA3335A",
    "mac address": "C8:FF:77:D9:4A:B2",
    "module hardware": "140762-01-07",
    "module bootloader": "-.-.-.-",
    "module software": "5221",
    "module nwp": "2.7.0.0",
    "product hardware": "306614-01-03",
    "product bootloader": "000000.00.00",
    "product software": "000027.19.59",
    "reset-source": "HIB"
}
```

#### Current Status

**Topic:** 455/PT4-EU-HKA3335A/status/current

**Message:**
```JSON
{
    "msg": "CURRENT-STATE",
    "time": "2017-11-25T15:39:54.000Z",
    "mode-reason": "RAPP",
    "state-reason": "MODE",
    "dial": "OFF",
    "rssi": "-21",
    "product-state": {
        "fmod": "OFF",
        "fnst": "OFF",
        "fnsp": "0001",
        "qtar": "0003",
        "oson": "OFF",
        "rhtm": "ON",
        "filf": "4025",
        "ercd": "NONE",
        "nmod": "OFF",
        "wacd": "NONE",
        "hmod": "OFF",
        "hmax": "2940",
        "hsta": "OFF",
        "ffoc": "ON",
        "tilt": "OK"
    },
    "scheduler": {
        "srsc": "27b5",
        "dstv": "0001",
        "tzid": "0001"
    }
}
```

> States of `product-state` dictionary are descibed in the *Data Values* section.

#### Current Environment Status

**Topic:** 455/PT4-EU-HKA3335A/status/current

**Message:**
```JSON
{
    "msg": "ENVIRONMENTAL-CURRENT-SENSOR-DATA",
    "time": "2017-11-25T15:39:55.000Z",
    "data": {
        "tact": "2948",
        "hact": "0054",
        "pact": "0000",
        "vact": "0002",
        "sltm": "OFF"
    }
}
```

> Values of `data` dictionary are descibed in the *Data Values* section.

#### Current Faults

**Topic:** 455/PT4-EU-HKA3335A/status/faults

**Message:**
```JSON
{
    "msg": "CURRENT-FAULTS",
    "time": "2017-11-25T18:06:16.005Z",
    "product-errors": {
        "cnfg": "OK",
        "stto": "OK",
        "hall": "OK",
        "hamp": "OK",
        "stal": "OK",
        "shrt": "OK",
        "wdog": "OK",
        "dsts": "OK",
        "vocs": "OK",
        "t&hs": "OK",
        "ibus": "OK",
        "ilss": "OK",
        "nvmw": "OK",
        "nvmr": "OK",
        "hioc": "OK",
        "hilc": "OK",
        "htri": "OK",
        "hamb": "OK",
        "povi": "OK",
        "htcf": "OK",
        "hvmi": "OK",
        "tilt": "OK",
        "wfhb": "OK",
        "wfcp": "OK"
    },
    "product-warnings": {
        "fltr": "OK"
    },
    "module-errors": {
        "szme": "OK",
        "szmw": "OK",
        "szps": "OK",
        "szpe": "OK",
        "szpw": "OK",
        "szed": "OK",
        "lspd": "OK",
        "szpi": "OK",
        "szpp": "OK",
        "szhv": "OK",
        "szbv": "OK",
        "szav": "OK"
    },
    "module-warnings": {
        "srnk": "FAIL",
        "stac": "OK",
        "strs": "OK",
        "srmi": "OK",
        "srmu": "OK",
        "nwcs": "OK",
        "nwts": "OK",
        "nwps": "OK",
        "nwss": "OK",
        "nwds": "OK"
    }
}
```

#### State Changed

**Topic:** 455/PT4-EU-HKA3335A/status/current

**Message:**
```JSON
{
    "msg": "STATE-CHANGE",
    "time": "2017-11-25T15:40:12.000Z",
    "mode-reason": "LAPP",
    "state-reason": "MODE",
    "product-state": {
        "fmod": [
            "OFF",
            "AUTO"
        ],
        "fnst": [
            "OFF",
            "FAN"
        ],
        "fnsp": [
            "0001",
            "AUTO"
        ],
        "qtar": [
            "0003",
            "0003"
        ],
        "oson": [
            "OFF",
            "OFF"
        ],
        "rhtm": [
            "ON",
            "ON"
        ],
        "filf": [
            "4025",
            "4025"
        ],
        "ercd": [
            "NONE",
            "NONE"
        ],
        "nmod": [
            "OFF",
            "OFF"
        ],
        "wacd": [
            "NONE",
            "NONE"
        ],
        "hmod": [
            "OFF",
            "OFF"
        ],
        "hmax": [
            "2940",
            "2940"
        ],
        "hsta": [
            "OFF",
            "OFF"
        ],
        "ffoc": [
            "ON",
            "ON"
        ],
        "tilt": [
            "OK",
            "OK"
        ]
    },
    "scheduler": {
        "srsc": "27b5",
        "dstv": "0001",
        "tzid": "0001"
    }
}
```
> `product-state` values correspond to [`old_state`, `new_state`]. 
> These state are descibed in the *Data Values* section.

#### Fault Changes

**Topic:** 455/PT4-EU-HKA3335A/status/faults

**Message:**
```JSON
{
    "msg": "FAULTS-CHANGE",
    "time": "2017-11-25T18:06:16.004Z",
    "product-errors": {},
    "product-warnings": {},
    "module-errors": {},
    "module-warnings": {
        "srnk": [
            "FAIL",
            "FAIL"
        ]
    }
}
```

### Sofware → Dyson

#### Request Current State

**Topic:** 455/PT4-EU-HKA3335A/command

**Message:**
```JSON
{
    "msg": "REQUEST-CURRENT-STATE",
    "time": "2017-11-25T15:40:00Z"
}
```

#### Set State

**Topic:** 455/PT4-EU-HKA3335A/command

**Message:** 
```JSON
{
    "msg": "STATE-SET",
    "time": "2017-11-25T15:40:36Z",
    "mode-reason": "LAPP",
    "data": {
        "fnsp": "0002",
        "fmod": "OFF",
        "oson": "OFF",
        "sltm": "OFF",
        "rhtm": "ON",
        "rstf": "STET",
        "qtar": "0003",
        "nmod": "OFF",
        "ffoc": "ON",
        "hmod": "OFF",
        "hmax": "2940"
    }
}
```

> Values of `data` dictionary are descibed in the *Data Values* section.

### Data Values

| Key    | Meaning                | Values                       |
|--------|------------------------|------------------------------|
| `fnsp` | Fan Speed              | `0001` to `0009`             |
| `fmod` | Fan Mode               | `OFF`, `AUTO`, `FAN`         |
| `fnst` | Fan Status             | `OFF`, `FAN`                 |
| `oson` | Oscillation            | `OFF`, `ON`                  |
| `sltm` | Sleep Timer            | `OFF`, `0015` (= 15 minutes) |
| `rhtm` | Continuous Monitoring  | `OFF`, `ON`                  |
| `filf` | Filter life            | `4025` (= 4025 hours)        |
| `rstf` | *Reset Filter (?)*     | *Unknown*                    |
| `qtar` | Air Quality Target     | `0004`, `003`, `0001` (smaller is better)
| `nmod` | Night Mode             | `OFF`, `ON`                  |
| `ffoc` | Focus                  | `OFF`, `ON`                  |
| `hmod` | Hot Mode               | `OFF`, `HEAT`                |
| `hmax` | Maximum Temperature    | `2980` (= 298 Kelvin)        |
| `hmod` | Hot Status             | `OFF`, `HEAT`                |
| `ercd` | *Unknown*              | `NONE`, *Unknown*            |
| `wacd` | *Unknown*              | `NONE`, *Unknown*            |
| `tilt` | *Unknown*              | `OK`, *Unknown*              |
| `tact` | Current temperature    | `2943` (=294.3 Kelvin)       |
| `hact` | Current humidity       | `0054` (= 54%)               |
| `pact` | *Unknown*              | `0000`, *Unknown*            |
| `vact` | *Unknown*              | `0002`, *Unknown*            |