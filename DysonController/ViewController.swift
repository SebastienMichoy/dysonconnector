//
//  ViewController.swift
//  DysonController
//
//  Created by Sébastien MICHOY on 11/25/17.
//  Copyright © 2017 Sébastien MICHOY. All rights reserved.
//

import Cocoa
import CocoaMQTT

class ViewController: NSViewController {

  // MARK: Init
  
  required init?(coder: NSCoder) {
    let clientId = "CocoaMQTT-" + String(ProcessInfo().processIdentifier)
    self.mqtt = CocoaMQTT(clientID: clientId, host: "192.168.0.30", port: 1883)
    
    super.init(coder: coder)
    
    self.mqtt.username = "PT4-EU-HKA3335A"
    self.mqtt.password = "5+AVgxQI2zYN/lOWLw3hRSHtMXmPk3ugKDYvupwkD0SFt+X+OmRVmkVQsBgpHlsjZiWjp4pRQ9N5s4lQJrIxMA=="
    
    self.mqtt.keepAlive = 60
    self.mqtt.delegate = self
    
    self.mqtt.connect()
  }
  
  // MARK: View Life Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }

  // MARK: Properties
  
  let mqtt: CocoaMQTT
  
  // MARK: Actions
  
  @IBAction func cool02ButtonClicked(_ sender: NSButton) {
    self.setCool02()
  }
  
  @IBAction func cool09ButtonClicked(_ sender: NSButton) {
    self.setCool09()
  }
  
  @IBAction func coolAutoButtonClicked(_ sender: NSButton) {
    self.setCoolAuto()
  }
  
  @IBAction func offButtonClicked(_ sender: NSButton) {
    self.turnOff()
  }
  // MARK: Calls
  
  func requestCurrentState() {
    let message = "{\"msg\": \"REQUEST-CURRENT-STATE\", \"time\": \"\(self.currentDate)\"}"
    self.mqtt.publish("455/PT4-EU-HKA3335A/command", withString: message)
    
//    let message = "{\"msg\": \"JOIN-NETWORK\", \"time\": \"\(self.currentDate)\", \"ssid\": \">Enter SSID here<\", \"password\": \"IsItThePass2GetTheInternet?\", \"requestId\": \"0123456789ABCDEF\"}"
//    self.mqtt.publish("455/initialconnection/command", withString: message)
//
//    let message2 = "{\"msg\": \"CLOSE-ACCESS-POINT\", \"time\": \"\(self.currentDate)\"}"
//    self.mqtt.publish("455/initialconnection/command", withString: message2)
  }
  
  func setCool02() {
    let message = "{\"msg\": \"STATE-SET\", \"time\": \"\(self.currentDate)\", \"mode-reason\": \"LAPP\", \"data\": { \"fnsp\": \"0002\", \"fmod\": \"FAN\", \"oson\": \"OFF\", \"sltm\": \"OFF\", \"rhtm\": \"ON\", \"rstf\": \"STET\", \"qtar\": \"0003\", \"nmod\": \"OFF\", \"ffoc\": \"ON\", \"hmod\": \"OFF\", \"hmax\": \"2940\"}}"
    self.mqtt.publish("455/PT4-EU-HKA3335A/command", withString: message)
  }
  
  func setCool09() {
    let message = "{\"msg\": \"STATE-SET\", \"time\": \"\(self.currentDate)\", \"mode-reason\": \"LAPP\", \"data\": { \"fnsp\": \"0009\", \"fmod\": \"FAN\", \"oson\": \"OFF\", \"sltm\": \"OFF\", \"rhtm\": \"ON\", \"rstf\": \"STET\", \"qtar\": \"0003\", \"nmod\": \"OFF\", \"ffoc\": \"ON\", \"hmod\": \"OFF\", \"hmax\": \"2940\"}}"
    self.mqtt.publish("455/PT4-EU-HKA3335A/command", withString: message)
  }
  
  func setCoolAuto() {
    let message = "{\"msg\": \"STATE-SET\", \"time\": \"\(self.currentDate)\", \"mode-reason\": \"LAPP\", \"data\": { \"fnsp\": \"0001\", \"fmod\": \"AUTO\", \"oson\": \"OFF\", \"sltm\": \"OFF\", \"rhtm\": \"ON\", \"rstf\": \"STET\", \"qtar\": \"0003\", \"nmod\": \"OFF\", \"ffoc\": \"ON\", \"hmod\": \"OFF\", \"hmax\": \"2940\"}}"
    self.mqtt.publish("455/PT4-EU-HKA3335A/command", withString: message)
  }
  
  func turnOff() {
    let message = "{\"msg\": \"STATE-SET\", \"time\": \"\(self.currentDate)\", \"mode-reason\": \"LAPP\", \"data\": { \"fnsp\": \"0001\", \"fmod\": \"OFF\", \"oson\": \"OFF\", \"sltm\": \"OFF\", \"rhtm\": \"ON\", \"rstf\": \"STET\", \"qtar\": \"0003\", \"nmod\": \"OFF\", \"ffoc\": \"ON\", \"hmod\": \"OFF\", \"hmax\": \"2940\"}}"
    self.mqtt.publish("455/PT4-EU-HKA3335A/command", withString: message)
  }
  
  // MARK: Helper
  
  var currentDate: String {
    let dateFormatter = ISO8601DateFormatter()
    return dateFormatter.string(from: Date())
  }
  
}

extension ViewController: CocoaMQTTDelegate {
  
  func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck) {
    DispatchQueue.main.async {
      print("Connected")
      
      self.mqtt.subscribe("455/PT4-EU-HKA3335A/status/current")
      self.mqtt.subscribe("455/PT4-EU-HKA3335A/status/faults")
      self.mqtt.subscribe("455/PT4-EU-HKA3335A/status/connection")
      self.mqtt.subscribe("455/PT4-EU-HKA3335A/status/software")
      self.mqtt.subscribe("455/PT4-EU-HKA3335A/status/summary")
      
//      self.mqtt.subscribe("455/initialconnection/credentials")
      
      DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        self.requestCurrentState()
      }
    }
  }
  
  func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16) {
    DispatchQueue.main.async {
      print("Did Publish: \(message.string ?? "NO CONTENT")")
    }
  }
  
  func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16) {
    
  }
  
  func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ) {
    DispatchQueue.main.async {
      print("Did Reveive: \(message.string ?? "NO CONTENT")")
    }
  }
  
  func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String) {
    DispatchQueue.main.async {
      print("Did Subscribe to: \(topic)")
    }
  }
  
  func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String) {
    
  }
  
  func mqttDidPing(_ mqtt: CocoaMQTT) {
    
  }
  
  func mqttDidReceivePong(_ mqtt: CocoaMQTT) {
    
  }
  
  func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?) {
    
  }
}
